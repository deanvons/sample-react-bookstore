//import logo from './logo.svg';
import './App.css';
import AppHeader from './components/AppHeader'
import BookList from './components/BookList'

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <AppHeader />
        <BookList />
        <img src="https://memegenerator.net/img/instances/57809725.jpg" className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
