import React from 'react'

function AppHeader() {
    return (
        <div>
            <h1>The Citadel</h1>
        </div>
    )
}

export default AppHeader