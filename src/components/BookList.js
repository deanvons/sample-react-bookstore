import { useState, useEffect } from 'react'
import bookData from '../utils/books'

function BookList() {
    console.log('App.init')
    const [books, setBooks] = useState([])

    useEffect(() => {
        console.log('useEffect()')
        setBooks(bookData)
        return () => {

        }
    }, [])

    console.log(books)

    return (
        <div>
            <p>{books[0].title}</p>
        </div>
    )
}

export default BookList